#include <glimac/SDLWindowManager.hpp>
#include <GL/glew.h>
#include <iostream>
#include <glimac/Program.hpp>
#include <glimac/FilePath.hpp>
#include <glimac/glm.hpp>

class Vertex2DColor {
  public:
    glm::vec2 position;
    glm::vec3 color;
    Vertex2DColor();
    Vertex2DColor( glm::vec2 pos,  glm::vec3 col );
};

Vertex2DColor::Vertex2DColor( glm::vec2 pos,  glm::vec3 col ) {
  position = pos;
  color = col;
}

using namespace glimac;

int main(int argc, char** argv) {
  // Initialize SDL and open a window
  SDLWindowManager windowManager(800, 600, "GLImac");
  
  // Initialize glew for OpenGL3+ support
  GLenum glewInitError = glewInit();
  if(GLEW_OK != glewInitError) {
    std::cerr << glewGetErrorString(glewInitError) << std::endl;
    return EXIT_FAILURE;
  }

  FilePath applicationPath(argv[0]);
  Program program = loadProgram(applicationPath.dirPath() + "shaders/triangle.vs.glsl", applicationPath.dirPath() + "shaders/triangle.fs.glsl");
  program.use();

  std::cout << "OpenGL Version : " << glGetString(GL_VERSION) << std::endl;
  std::cout << "GLEW Version : " << glewGetString(GLEW_VERSION) << std::endl;

  /*********************************
   * HERE SHOULD COME THE INITIALIZATION CODE
   *********************************/

  // Création d'un VBO
  GLuint vbo;
  glGenBuffers(1, &vbo);

  // Binding d'un VBO sur la cible GL_ARRAY_BUFFER
  glBindBuffer(GL_ARRAY_BUFFER, vbo);

  // Remplissage du VBO avec les points du triangle
  Vertex2DColor vertices[] = {
    Vertex2DColor(glm::vec2(-0.5, -0.5), glm::vec3(1, 0, 0)),
    Vertex2DColor(glm::vec2(0.5, -0.5), glm::vec3(1, 0, 0)),
    Vertex2DColor(glm::vec2(0.5, 0.5), glm::vec3(1, 0, 0)),
    Vertex2DColor(glm::vec2(-0.5, 0.5), glm::vec3(1, 0, 0))
  };

  glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(Vertex2DColor), vertices, GL_STATIC_DRAW);

  // Débinder le VBO
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  // Creation du IBO Index Buffer Object
  GLuint ibo;
  glGenBuffers(1, &ibo);

  // On bind sur GL_ELEMENT_ARRAY_BUFFER (pour les IBO)
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);

  // Tableau d'indices
  uint32_t indices[] = {
    0, 1, 2, 2, 0, 3
  };

  // Remplissage de l'IBO avec les indices
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(uint32_t), indices, GL_STATIC_DRAW);

  // Debind de l'IBO
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

  // Spécification de sommet avec le Vertex Array Object (VAO)
  GLuint vao;
  glGenVertexArrays(1, &vao);

  // Binding du VAO
  glBindVertexArray(vao);

  // On bind l'IBO sur GL_ELEMENT_ARRAY_BUFFER car un VAO est bindé ca va enregistrer l'IBO dans le VAO
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,  ibo);

  // Activation des attributs de vertex
  const GLuint VERTEX_ATTR_POSITION = 3;
  glEnableVertexAttribArray(VERTEX_ATTR_POSITION);
  const GLuint VERTEX_ATTR_COLOR = 8;
  glEnableVertexAttribArray(VERTEX_ATTR_COLOR);

  // Spécification des attributs de vertex
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glVertexAttribPointer(VERTEX_ATTR_POSITION, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex2DColor), (const GLvoid*) offsetof(Vertex2DColor, position));
  glVertexAttribPointer(VERTEX_ATTR_COLOR, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex2DColor), (const GLvoid*) offsetof(Vertex2DColor, color));
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  // Debinder le VAO
  glBindVertexArray(0);

  // Debinder le IBO
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

  // Application loop:
  bool done = false;
  while(!done) {
    // Event loop:
    SDL_Event e;
    while(windowManager.pollEvent(e)) {
      if(e.type == SDL_QUIT) {
        done = true; // Leave the loop after this iteration
      }
    }

    /*********************************
     * HERE SHOULD COME THE RENDERING CODE
     *********************************/
    // Nettoyer la fenêtre
    glClear(GL_COLOR_BUFFER_BIT);

    // Dessiner en utilisant le VAO
    glBindVertexArray(vao);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);

    // Update the display
    windowManager.swapBuffers();
  }

  glDeleteVertexArrays(1, &vao);
  glDeleteBuffers(1,  &vbo);
  glDeleteBuffers(1,  &ibo);

  return EXIT_SUCCESS;
}
