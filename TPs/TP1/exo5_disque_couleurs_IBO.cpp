#include <glimac/SDLWindowManager.hpp>
#include <GL/glew.h>
#include <iostream>
#include <glimac/Program.hpp>
#include <glimac/FilePath.hpp>
#include <vector>
#include <glimac/glm.hpp>

class Vertex2DColor {
  public:
    glm::vec2 position;
    glm::vec3 color;
    Vertex2DColor();
    Vertex2DColor( glm::vec2 pos,  glm::vec3 col );
};

Vertex2DColor::Vertex2DColor( glm::vec2 pos,  glm::vec3 col ) {
  position = pos;
  color = col;
}

using namespace glimac;

int main(int argc, char** argv) {
  // Initialize SDL and open a window
  SDLWindowManager windowManager(800, 600, "GLImac");
  
  // Initialize glew for OpenGL3+ support
  GLenum glewInitError = glewInit();
  if(GLEW_OK != glewInitError) {
    std::cerr << glewGetErrorString(glewInitError) << std::endl;
    return EXIT_FAILURE;
  }

  FilePath applicationPath(argv[0]);
  Program program = loadProgram(applicationPath.dirPath() + "shaders/triangle.vs.glsl", applicationPath.dirPath() + "shaders/triangle.fs.glsl");
  program.use();

  std::cout << "OpenGL Version : " << glGetString(GL_VERSION) << std::endl;
  std::cout << "GLEW Version : " << glewGetString(GLEW_VERSION) << std::endl;

  /*********************************
   * HERE SHOULD COME THE INITIALIZATION CODE
   *********************************/

  // Création d'un VBO
  GLuint vbo;
  glGenBuffers(1, &vbo);

  // Binding d'un VBO sur la cible GL_ARRAY_BUFFER
  glBindBuffer(GL_ARRAY_BUFFER, vbo);

  // Remplissage du VBO avec les points du triangle
  std::vector<Vertex2DColor> vertices;

  int N = 20;
  GLfloat x, y;
  GLfloat teta = 0;
  GLfloat angle = 2 * glm::pi<float>()/N;
  glm::vec3 color = glm::vec3(1, 1, 1);

  vertices.push_back(Vertex2DColor(glm::vec2(0, 0), color));

  for (teta = 0; teta <= 2 * glm::pi<float>(); teta+=angle) {

    glm::vec2 position = glm::vec2(glm::vec2(0.5f * glm::cos(teta), 0.5f * glm::sin(teta)));

    vertices.push_back(Vertex2DColor(position, color));
  }


  glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex2DColor), vertices.data(), GL_STATIC_DRAW);

  // Débinder le VBO
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  // creation de l'IBO
  GLuint ibo;
  glGenBuffers(1, &ibo);

  // On bind l'IBO
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,  ibo);

  // Création du tableau d'indices
  uint32_t indices[N * 3];

  indices[0] = 0;
  indices[1] = 1;
  indices[2] = 2;

  for (int i = 1; i <= N-1; ++i) {
    indices[i*3] = 0;
    indices[i*3+1] = indices[i*3-1];
    if ( indices[i*3-1] == 20 ) indices[i*3+2]=1;
    else {
      indices[i*3+2] = indices[i*3-1] + 1;
    }
  }

  glBufferData(GL_ELEMENT_ARRAY_BUFFER, N*3*sizeof(uint32_t), indices, GL_STATIC_DRAW);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

  // Spécification de sommet avec le Vertex Array Object (VAO)
  GLuint vao;
  glGenVertexArrays(1, &vao);

  // Binding du VAO
  glBindVertexArray(vao);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);


  // Activation des attributs de vertex
  const GLuint VERTEX_ATTR_POSITION = 3;
  glEnableVertexAttribArray(VERTEX_ATTR_POSITION);
  const GLuint VERTEX_ATTR_COLOR = 8;
  glEnableVertexAttribArray(VERTEX_ATTR_COLOR);

  // Spécification des attributs de vertex
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glVertexAttribPointer(VERTEX_ATTR_POSITION, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex2DColor), (const GLvoid*) offsetof(Vertex2DColor, position));
  glVertexAttribPointer(VERTEX_ATTR_COLOR, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex2DColor), (const GLvoid*) offsetof(Vertex2DColor, color));
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  // Debinder le VAO
  glBindVertexArray(0);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);


  // Application loop:
  bool done = false;
  while(!done) {
    // Event loop:
    SDL_Event e;
    while(windowManager.pollEvent(e)) {
      if(e.type == SDL_QUIT) {
        done = true; // Leave the loop after this iteration
      }
    }

    /*********************************
     * HERE SHOULD COME THE RENDERING CODE
     *********************************/
    // Nettoyer la fenêtre
    glClear(GL_COLOR_BUFFER_BIT);

    // Dessiner en utilisant le VAO
    glBindVertexArray(vao);
    glDrawElements(GL_TRIANGLES, 3*N, GL_UNSIGNED_INT, 0);

    glBindVertexArray(0);

    // Update the display
    windowManager.swapBuffers();
  }

  glDeleteVertexArrays(1, &vao);
  glDeleteBuffers(1,  &vbo);

  return EXIT_SUCCESS;
}
