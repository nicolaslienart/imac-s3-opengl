#version 330

in vec2 vVertexPosition;

out vec3 fFragColor;

vec2 complexSqrt(vec2 z)
{
  vec2 res;
  res.x = pow(z.x, 2) - pow(z.y, 2);
  res.y = 2 * z.y * z.x;
  return res;
}

void main() {
  vec2 z = vVertexPosition;
  float M;
  int N = 107;
  bool white = false;
  int i;
  for (i=0; i<N && white != true; i++) {
    M = length(z);
    if (M > 2.f) white = true;
    z = complexSqrt(z) + z;
  }

  //if (white) fFragColor = vec3(1,1,1);
  if (white) fFragColor = vec3(float(i)/N);
  else fFragColor = vec3(0, 0, 0);
}
