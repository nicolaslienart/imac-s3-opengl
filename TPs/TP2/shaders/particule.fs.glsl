#version 330 core

in vec3 vFragColor;
in vec2 vVertexPosition;
in vec2 middle;

out vec3 fFragColor;

void main() {
  //fFragColor = vFragColor;
  vec2 middle = vec2(0.f, 0.f);

  float a = 14.29 * exp( -52.3 * pow( distance( vVertexPosition, middle ), 2 ) );
  fFragColor = vec3(vFragColor.x*a, vFragColor.y*a, vFragColor.z*a);
}
