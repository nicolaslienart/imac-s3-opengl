#version 330

in vec2 vTexCoord;

uniform vec3 uColor;
uniform sampler2D uTexture;

out vec3 fFragColor;

void main() {
  vec3 textureTri = texture(uTexture, vTexCoord).xyz;
  fFragColor = textureTri;
}
