#version 330

layout(location = 0) in vec2 aVertexPosition;
layout(location = 1) in vec2 aTexCoord;

uniform mat3 uModelMatrix;

mat3 rotate(float teta)
{
  return mat3(vec3(cos(teta), sin(teta), 0), vec3(-sin(teta), cos(teta), 0), vec3(0, 0, 1));
}

out vec2 vTexCoord;

void main() {
  vTexCoord = aTexCoord;

  vec2 transformed = (uModelMatrix * vec3(aVertexPosition, 1)).xy;
  gl_Position = vec4(transformed, 0, 1);
}
