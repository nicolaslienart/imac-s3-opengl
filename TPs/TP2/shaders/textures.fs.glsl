#version 330 core

in vec3 vFragColor;
in vec2 vVertexPosition;
in vec2 middle;

out vec3 fFragColor;

void main() {
  //fFragColor = vFragColor;
  vec2 middle = vec2(0.f, 0.f);

  //float a = length(fract(5.0 * vVertexPosition));
  float a = smoothstep(0.4, 0.5, max(abs(fract(8.0 * vVertexPosition.x -0.5 * mod(floor(8.0 * vVertexPosition.y), 2.0))-0.5), abs(fract(8.0 * vVertexPosition.y)-0.5)));
  //float a = smoothstep(3.0, 0.32, length(fract(5.0 * vVertexPosition) -0.5));
  //float a = mod(floor(10.0 * vVertexPosition.x) + floor(10.0 * vVertexPosition.y), 2.0);
  //float a = length(abs( fract(5.0 * vVertexPosition) * 2.0 - 1.0));
  fFragColor = vec3(vFragColor.x*a, vFragColor.y*a, vFragColor.z*a);
}
