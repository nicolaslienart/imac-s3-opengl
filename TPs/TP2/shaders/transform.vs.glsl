#version 330 core

layout(location = 0) in vec2 aVertexPosition;
layout(location = 1) in vec3 aVertexColor;

out vec3 vFragColor;
out vec2 middle;
out vec2 vVertexPosition;

mat3 scale(float sx, float sy)
{
  return mat3(vec3(sx, 0, 0), vec3(0, sy, 0), vec3(0, 0, 1));
}

mat3 rotate(float teta)
{
  return mat3(vec3(cos(teta), sin(teta), 0), vec3(-sin(teta), cos(teta), 0), vec3(0, 0, 1));
}

mat3 translate(float tx, float ty)
{
  return mat3(vec3(1, 0, 0), vec3(0, 1, 0), vec3(tx, ty, 1));
}

void main() {
  vFragColor = aVertexColor;
  vVertexPosition = aVertexPosition;
  vec2 middle = vec2(0, 0);
  //gl_Position = vec4((aVertexPosition + vec2(0.5f, 0.5f)) * 0.5f, 0, 1);
  //gl_Position = vec4(aVertexPosition * vec2(2.f, 0.5f), 0, 1);

  //vec2 transformed = (M * vec3(aVertexPosition, 1)).xy;
  //vec2 transformed = ( vec3(aVertexPosition, 1)).xy;
  //vec2 transformed = (scale(0.5f, 0.5f) * vec3(aVertexPosition, 1)).xy;
  //vec2 transformed = (scale(0.5f, 0.5f) * vec3(aVertexPosition, 1)).xy;
  //vec2 transformed = (scale(1.0f, 0.5f) * rotate(radians(90.f))  * vec3(aVertexPosition, 1)).xy;
  //vec2 transformed = (rotate(24) * vec3(aVertexPosition, 1)).xy;
  vec2 transformed = (translate(0.5, 0) * rotate(radians(45.0)) * scale(0.5, 0.5) * vec3(aVertexPosition, 1)).xy;
  //vec2 transformed = ( rotate(radians(-45.0)) * scale(2, 0.5) * translate(0, 0.5) * vec3(aVertexPosition, 1) ).xy;
  gl_Position = vec4(transformed, 0, 1);
}

