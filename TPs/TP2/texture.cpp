#include <glimac/SDLWindowManager.hpp>
#include <glimac/Image.hpp>
#include <GL/glew.h>
#include <iostream>
#include <glimac/Program.hpp>
#include <glimac/FilePath.hpp>

using namespace glimac;

struct Vertex2DUV {
  glm::vec2 position;
  glm::vec2 tex_coord;
};

glm::mat3 scale(float sx, float sy)
{
  return glm::mat3(glm::vec3(sx, 0, 0), glm::vec3(0, sy, 0), glm::vec3(0, 0, 1));
}

glm::mat3 rotate(float teta)
{
  return glm::mat3(glm::vec3(cos(teta), sin(teta), 0), glm::vec3(-sin(teta), cos(teta), 0), glm::vec3(0, 0, 1));
}

glm::mat3 translate(float tx, float ty)
{
  return glm::mat3(glm::vec3(1, 0, 0), glm::vec3(0, 1, 0), glm::vec3(tx, ty, 1));
}

int main(int argc, char** argv) {
  // Initialize SDL and open a window
  SDLWindowManager windowManager(800, 600, "GLImac");

  // Initialize glew for OpenGL3+ support
  GLenum glewInitError = glewInit();
  if(GLEW_OK != glewInitError) {
    std::cerr << glewGetErrorString(glewInitError) << std::endl;
    return EXIT_FAILURE;
  }

  FilePath applicationPath(argv[0]);
  Program program = loadProgram(applicationPath.dirPath() + "shaders/tex2D.vs.glsl", applicationPath.dirPath() + "shaders/tex2D.fs.glsl");
  program.use();

  std::cout << "OpenGL Version : " << glGetString(GL_VERSION) << std::endl;
  std::cout << "GLEW Version : " << glewGetString(GLEW_VERSION) << std::endl;

  /*********************************
   * HERE SHOULD COME THE INITIALIZATION CODE
   *********************************/

  std::unique_ptr<Image> triforce_image = loadImage("/home/nicolas/Documents/IMAC/SEM3/imac-s3-opengl/TPs/TP2/assets/textures/triforce.png");

  GLuint triforce_tex;
  glGenTextures(1, &triforce_tex);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, triforce_image->getWidth(), triforce_image->getHeight(), 0, GL_RGBA, GL_FLOAT, triforce_image->getPixels());
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glBindTexture(GL_TEXTURE_2D, 0);

  glBindTexture(GL_TEXTURE_2D, triforce_tex);

  GLint uModelMatrixLocation = glGetUniformLocation(program.getGLId(), "uModelMatrix");
  GLint uColorLocation = glGetUniformLocation(program.getGLId(), "uColor");
  GLint uTextureLocation = glGetUniformLocation(program.getGLId(), "uTexture");
  glUniform1i(uTextureLocation, 0);

  GLuint vbo;
  glGenBuffers(1, &vbo);

  glBindBuffer(GL_ARRAY_BUFFER, vbo);

  Vertex2DUV triangle[] = {
    glm::vec2(-1.f, -1.f), glm::vec2(0.f, 1.f),
    glm::vec2(1.f, -1.f), glm::vec2(0.5f, 0.f),
    glm::vec2(0.f, 1.f), glm::vec2(1.f, 1.f),
  };

  glBufferData(GL_ARRAY_BUFFER, 3 * sizeof(Vertex2DUV), triangle, GL_STATIC_DRAW);

  glBindBuffer(GL_ARRAY_BUFFER, 0);

  GLuint vao;
  glGenVertexArrays(1, &vao);

  glBindVertexArray(vao);

  const GLuint VERTEX_ATTR_POSITION = 0;
  const GLuint VERTEX_ATTR_TEX_COORD = 1;

  glEnableVertexAttribArray(VERTEX_ATTR_POSITION);
  glEnableVertexAttribArray(VERTEX_ATTR_TEX_COORD);

  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glVertexAttribPointer(VERTEX_ATTR_POSITION, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
  glVertexAttribPointer(VERTEX_ATTR_TEX_COORD, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (const GLvoid*) (2 * sizeof(GLfloat)));
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);


  // Application loop:
  bool done = false;
  while(!done) {
    // Event loop:
    SDL_Event e;
    while(windowManager.pollEvent(e)) {
      if(e.type == SDL_QUIT) {
        done = true; // Leave the loop after this iteration
      }
    }

    /*********************************
     * HERE SHOULD COME THE RENDERING CODE
     *********************************/
    glClear(GL_COLOR_BUFFER_BIT);

    glBindVertexArray(vao);

    glm::mat3 rotation_m = rotate(float( SDL_GetTicks()/100%360 ));
    glm::mat3 translate_NW_m = translate(-0.5, 0.5);
    glm::mat3 translate_SW_m = translate(-0.5, -0.5);
    glm::mat3 translate_NE_m = translate(0.5, 0.5);
    glm::mat3 translate_SE_m = translate(0.5, -0.5);
    glm::mat3 scale_m = scale(0.25, 0.25);

    glm::vec3 color_1(0,1,1);
    glm::vec3 color_2(1,1,1);
    glm::vec3 color_3(1,1,0);
    glm::vec3 color_4(1,0,1);


    glUniformMatrix3fv(uModelMatrixLocation, 1, GL_FALSE, glm::value_ptr( rotation_m * translate_NE_m * rotation_m * scale_m));
    glUniform3fv(uColorLocation, 1, glm::value_ptr(color_1));
    glDrawArrays(GL_TRIANGLES, 0, 3);
    glUniformMatrix3fv(uModelMatrixLocation, 1, GL_FALSE, glm::value_ptr( rotation_m * translate_NW_m * rotation_m * scale_m));
    glUniform3fv(uColorLocation, 1, glm::value_ptr(color_2));
    glDrawArrays(GL_TRIANGLES, 0, 3);
    glUniformMatrix3fv(uModelMatrixLocation, 1, GL_FALSE, glm::value_ptr( rotation_m * translate_SE_m * rotation_m * scale_m));
    glUniform3fv(uColorLocation, 1, glm::value_ptr(color_3));
    glDrawArrays(GL_TRIANGLES, 0, 3);
    glUniformMatrix3fv(uModelMatrixLocation, 1, GL_FALSE, glm::value_ptr( rotation_m * translate_SW_m * rotation_m * scale_m));
    glUniform3fv(uColorLocation, 1, glm::value_ptr(color_4));
    glBindTexture(GL_TEXTURE_2D, triforce_tex);

    glBindTexture(GL_TEXTURE_2D, triforce_tex);
    glDrawArrays(GL_TRIANGLES, 0, 3);
    glBindTexture(GL_TEXTURE_2D, 0);

    glBindVertexArray(0);


    // Update the display
    windowManager.swapBuffers();
  }

  glDeleteVertexArrays(1, &vao);
  glDeleteBuffers(1,  &vbo);
  glDeleteTextures(1, &triforce_tex);
  return EXIT_SUCCESS;
}
