#include <glimac/SDLWindowManager.hpp>
#include <glimac/Image.hpp>
#include <glimac/Sphere.hpp>
#include <GL/glew.h>
#include <iostream>
#include <glimac/common.hpp>
#include <glimac/Program.hpp>
#include <glimac/FilePath.hpp>
#include <vector>

using namespace glimac;

int main(int argc, char** argv) {
  // Initialize SDL and open a window
  SDLWindowManager windowManager(800, 600, "GLImac");

  // Initialize glew for OpenGL3+ support
  GLenum glewInitError = glewInit();
  if(GLEW_OK != glewInitError) {
    std::cerr << glewGetErrorString(glewInitError) << std::endl;
    return EXIT_FAILURE;
  }

  FilePath applicationPath(argv[0]);
  Program program = loadProgram(applicationPath.dirPath() + "shaders/3D.vs.glsl", applicationPath.dirPath() + "shaders/tex3D.fs.glsl");
  program.use();

  GLint uMVPMatrix = glGetUniformLocation(program.getGLId(), "uMVPMatrix");
  GLint uMVMatrix = glGetUniformLocation(program.getGLId(), "uMVMatrix");
  GLint uNormalMatrix = glGetUniformLocation(program.getGLId(), "uNormalMatrix");

  GLint uTextureLocation = glGetUniformLocation(program.getGLId(), "uTexture");
  glUniform1i(uTextureLocation, 0);

  std::cout << "OpenGL Version : " << glGetString(GL_VERSION) << std::endl;
  std::cout << "GLEW Version : " << glewGetString(GLEW_VERSION) << std::endl;

  /*********************************
   * HERE SHOULD COME THE INITIALIZATION CODE
   *********************************/

  std::unique_ptr<Image> earth_map = loadImage("/home/nicolas/Documents/IMAC/SEM3/imac-s3-opengl/TPs/TP3/assets/textures/EarthMap.jpg");
  std::unique_ptr<Image> moon_map = loadImage("/home/nicolas/Documents/IMAC/SEM3/imac-s3-opengl/TPs/TP3/assets/textures/MoonMap.jpg");

  GLuint earth_map_tex, moon_map_tex;
  glGenTextures(1, &earth_map_tex);
  glGenTextures(1, &moon_map_tex);

  glBindTexture(GL_TEXTURE_2D, earth_map_tex);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, earth_map->getWidth(), earth_map->getHeight(), 0, GL_RGBA, GL_FLOAT, earth_map->getPixels());
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  glBindTexture(GL_TEXTURE_2D, moon_map_tex);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, moon_map->getWidth(), moon_map->getHeight(), 0, GL_RGBA, GL_FLOAT, moon_map->getPixels());
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glBindTexture(GL_TEXTURE_2D, 0);

  std::vector<glm::vec3> positions;
  for ( int i = 0; i < 32; i++ ) {
    positions.push_back(glm::sphericalRand(2.f));
  }

  glm::vec3 test = glm::sphericalRand(2.f);

  Sphere sphere(1, 32, 16);
  // Création d'un VBO
  GLuint vbo;
  glGenBuffers(1, &vbo);

  // Binding d'un VBO sur la cible GL_ARRAY_BUFFER
  glBindBuffer(GL_ARRAY_BUFFER, vbo);

  glBufferData(GL_ARRAY_BUFFER, sphere.getVertexCount() * sizeof(ShapeVertex), sphere.getDataPointer(), GL_STATIC_DRAW);

  // Débinder le VBO
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  // Spécification de sommet avec le Vertex Array Object (VAO)
  GLuint vao;
  glGenVertexArrays(1, &vao);

  // Binding du VAO
  glBindVertexArray(vao);

  // Activation des attributs de vertex
  const GLuint VERTEX_ATTR_POSITION = 0;
  glEnableVertexAttribArray(VERTEX_ATTR_POSITION);
  const GLuint VERTEX_ATTR_NORMAL = 1;
  glEnableVertexAttribArray(VERTEX_ATTR_NORMAL);
  const GLuint VERTEX_ATTR_TEXCOORD = 2;
  glEnableVertexAttribArray(VERTEX_ATTR_TEXCOORD);

  // Spécification des attributs de vertex
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glVertexAttribPointer(VERTEX_ATTR_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const void *) 0);
  glVertexAttribPointer(VERTEX_ATTR_NORMAL, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const void *) offsetof(ShapeVertex, normal));
  glVertexAttribPointer(VERTEX_ATTR_TEXCOORD, 2, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const void *) offsetof(ShapeVertex, texCoords));
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  // Debinder le VAO
  glBindVertexArray(0);

  glEnable(GL_DEPTH_TEST);

  glm::mat4 ProjMatrix;
  glm::mat4 MVMatrix;
  glm::mat4 NormalMatrix;

  ProjMatrix = glm::perspective(glm::radians(70.f), 8.f/6.f, 0.1f, 100.f);
  MVMatrix = glm::translate(glm::mat4(), glm::vec3(0.f, 0.f, -5.f));
  NormalMatrix = glm::transpose(glm::inverse(MVMatrix));

  // Application loop:
  bool done = false;
  while(!done) {
    // Event loop:
    SDL_Event e;
    while(windowManager.pollEvent(e)) {
      if(e.type == SDL_QUIT) {
        done = true; // Leave the loop after this iteration
      }
    }

    /*********************************
     * HERE SHOULD COME THE RENDERING CODE
     *********************************/
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


    glUniformMatrix4fv(uMVPMatrix, 1, GL_FALSE, glm::value_ptr(ProjMatrix * MVMatrix));
    glUniformMatrix4fv(uMVMatrix, 1, GL_FALSE, glm::value_ptr(MVMatrix));
    glUniformMatrix4fv(uNormalMatrix, 1, GL_FALSE, glm::value_ptr(NormalMatrix));

    glm::mat4 earthMVMatrix = glm::rotate(NormalMatrix, windowManager.getTime(), glm::vec3(0, 1, 0));
    glUniformMatrix4fv(uMVMatrix, 1, GL_FALSE, glm::value_ptr(earthMVMatrix));

    glBindVertexArray(vao);
    glBindTexture(GL_TEXTURE_2D, earth_map_tex);
    glDrawArrays(GL_TRIANGLES, 0, sphere.getVertexCount());
    glBindTexture(GL_TEXTURE_2D, 0);



    /*
    //glm::mat4 moonMVMatrix = glm::translate(glm::mat4(1), glm::vec3(0, 0, -5)); // Translation
    //moonMVMatrix = glm::rotate(moonMVMatrix, windowManager.getTime(), glm::vec3(0, 1, 0)); // Translation * Rotation
    for ( int i = 0; i < positions.size(); i++ ) {
      glm::mat4 moonMVMatrix = glm::rotate(glm::mat4(1), windowManager.getTime(), positions[i]); // Translation * Rotation
      moonMVMatrix = glm::translate(moonMVMatrix, positions[i]); // Translation * Rotation * Translation
      moonMVMatrix = glm::scale(moonMVMatrix, glm::vec3(0.2, 0.2, 0.2)); // Translation * Rotation * Translation * Scale

      glUniformMatrix4fv(uMVMatrix, 1, GL_FALSE, glm::value_ptr(moonMVMatrix));
      glDrawArrays(GL_TRIANGLES, 0, sphere.getVertexCount());
      
    }
    */

    for ( int i = 0; i < positions.size(); i++ ) {
      glm::mat4 moonMVMatrix = glm::translate(glm::mat4(1), glm::vec3(0, 0, -5)); // Translation
      moonMVMatrix = glm::rotate(moonMVMatrix, windowManager.getTime() * positions[i].x, positions[i]); // Translation * Rotation
      moonMVMatrix = glm::translate(moonMVMatrix, glm::vec3(7, 0, 0)); // Translation * Rotation * Translation
      moonMVMatrix = glm::scale(moonMVMatrix, glm::vec3(0.2, 0.2, 0.2)); // Translation * Rotation * Translation * Scale

      glUniformMatrix4fv(uMVMatrix, 1, GL_FALSE, glm::value_ptr(moonMVMatrix));
      glBindTexture(GL_TEXTURE_2D, moon_map_tex);
      glDrawArrays(GL_TRIANGLES, 0, sphere.getVertexCount());
      glBindTexture(GL_TEXTURE_2D, 0);
    }


    glBindVertexArray(0);

    // Update the display
    windowManager.swapBuffers();
  }

  return EXIT_SUCCESS;
}
