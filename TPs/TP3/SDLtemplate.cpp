#include <glimac/SDLWindowManager.hpp>
#include <glimac/Sphere.hpp>
#include <GL/glew.h>
#include <iostream>
#include <glimac/common.hpp>
#include <glimac/Program.hpp>
#include <glimac/FilePath.hpp>

using namespace glimac;

int main(int argc, char** argv) {
  // Initialize SDL and open a window
  SDLWindowManager windowManager(800, 600, "GLImac");

  // Initialize glew for OpenGL3+ support
  GLenum glewInitError = glewInit();
  if(GLEW_OK != glewInitError) {
    std::cerr << glewGetErrorString(glewInitError) << std::endl;
    return EXIT_FAILURE;
  }

  FilePath applicationPath(argv[0]);
  Program program = loadProgram(applicationPath.dirPath() + "shaders/3D.vs.glsl", applicationPath.dirPath() + "shaders/normal.fs.glsl");
  program.use();

  GLint uMVPMatrix = glGetUniformLocation(program.getGLId(), "uMVPMatrix");
  GLint uMVMatrix = glGetUniformLocation(program.getGLId(), "uMVMatrix");
  GLint uNormalMatrix = glGetUniformLocation(program.getGLId(), "uNormalMatrix");

  std::cout << "OpenGL Version : " << glGetString(GL_VERSION) << std::endl;
  std::cout << "GLEW Version : " << glewGetString(GLEW_VERSION) << std::endl;

  /*********************************
   * HERE SHOULD COME THE INITIALIZATION CODE
   *********************************/

  Sphere sphere(1, 32, 16);
  // Création d'un VBO
  GLuint vbo;
  glGenBuffers(1, &vbo);

  // Binding d'un VBO sur la cible GL_ARRAY_BUFFER
  glBindBuffer(GL_ARRAY_BUFFER, vbo);

  glBufferData(GL_ARRAY_BUFFER, sphere.getVertexCount() * sizeof(ShapeVertex), sphere.getDataPointer(), GL_STATIC_DRAW);

  // Débinder le VBO
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  // Spécification de sommet avec le Vertex Array Object (VAO)
  GLuint vao;
  glGenVertexArrays(1, &vao);

  // Binding du VAO
  glBindVertexArray(vao);

  // Activation des attributs de vertex
  const GLuint VERTEX_ATTR_POSITION = 0;
  glEnableVertexAttribArray(VERTEX_ATTR_POSITION);
  const GLuint VERTEX_ATTR_NORMAL = 1;
  glEnableVertexAttribArray(VERTEX_ATTR_NORMAL);
  const GLuint VERTEX_ATTR_TEXCOORD = 2;
  glEnableVertexAttribArray(VERTEX_ATTR_TEXCOORD);

  // Spécification des attributs de vertex
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glVertexAttribPointer(VERTEX_ATTR_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const void *) 0);
  glVertexAttribPointer(VERTEX_ATTR_NORMAL, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const void *) offsetof(ShapeVertex, normal));
  glVertexAttribPointer(VERTEX_ATTR_TEXCOORD, 2, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const void *) offsetof(ShapeVertex, texCoords));
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  // Debinder le VAO
  glBindVertexArray(0);

  glEnable(GL_DEPTH_TEST);

  glm::mat4 ProjMatrix;
  glm::mat4 MVMatrix;
  glm::mat4 NormalMatrix;

  ProjMatrix = glm::perspective(glm::radians(70.f), 8.f/6.f, 0.1f, 100.f);
  MVMatrix = glm::translate(glm::mat4(), glm::vec3(0.f, 0.f, -5.f));
  NormalMatrix = glm::transpose(glm::inverse(MVMatrix));

  // Application loop:
  bool done = false;
  while(!done) {
    // Event loop:
    SDL_Event e;
    while(windowManager.pollEvent(e)) {
      if(e.type == SDL_QUIT) {
        done = true; // Leave the loop after this iteration
      }
    }

    /*********************************
     * HERE SHOULD COME THE RENDERING CODE
     *********************************/
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glUniformMatrix4fv(uMVPMatrix, 1, GL_FALSE, glm::value_ptr(ProjMatrix * MVMatrix));
    glUniformMatrix4fv(uMVMatrix, 1, GL_FALSE, glm::value_ptr(MVMatrix));
    glUniformMatrix4fv(uNormalMatrix, 1, GL_FALSE, glm::value_ptr(NormalMatrix));

    glBindVertexArray(vao);
    glDrawArrays(GL_TRIANGLES, 0, sphere.getVertexCount());



    //glm::mat4 moonMVMatrix = glm::translate(glm::mat4(1), glm::vec3(0, 0, -5)); // Translation
    //moonMVMatrix = glm::rotate(moonMVMatrix, windowManager.getTime(), glm::vec3(0, 1, 0)); // Translation * Rotation
    glm::mat4 moonMVMatrix = glm::rotate(glm::mat4(1), windowManager.getTime(), glm::vec3(0, 1, 0)); // Translation * Rotation
    moonMVMatrix = glm::translate(moonMVMatrix, glm::vec3(-2, 0, 0)); // Translation * Rotation * Translation
    moonMVMatrix = glm::scale(moonMVMatrix, glm::vec3(0.2, 0.2, 0.2)); // Translation * Rotation * Translation * Scale

    glUniformMatrix4fv(uMVMatrix, 1, GL_FALSE, glm::value_ptr(moonMVMatrix));
    glDrawArrays(GL_TRIANGLES, 0, sphere.getVertexCount());

    glBindVertexArray(0);

    // Update the display
    windowManager.swapBuffers();
  }

  return EXIT_SUCCESS;
}
