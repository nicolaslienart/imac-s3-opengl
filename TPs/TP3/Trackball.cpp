#include <glimac/SDLWindowManager.hpp>
#include <glimac/TrackballCamera.hpp>
#include <glimac/Image.hpp>
#include <glimac/Sphere.hpp>
#include <GL/glew.h>
#include <iostream>
#include <glimac/common.hpp>
#include <glimac/Program.hpp>
#include <glimac/FilePath.hpp>
#include <vector>


using namespace glimac;

struct EarthProgram {
  Program m_Program;

  GLint uMVPMatrix;
  GLint uMVMatrix;
  GLint uNormalMatrix;
  GLint uEarthTexture;
  GLint uCloudTexture;

  EarthProgram(const FilePath &applicationPath)
      : m_Program(loadProgram(applicationPath.dirPath() + "shaders/3D.vs.glsl",
                              applicationPath.dirPath() +
                                  "shaders/multiTex3D.fs.glsl")) {
    uMVPMatrix = glGetUniformLocation(m_Program.getGLId(), "uMVPMatrix");
    uMVMatrix = glGetUniformLocation(m_Program.getGLId(), "uMVMatrix");
    uNormalMatrix = glGetUniformLocation(m_Program.getGLId(), "uNormalMatrix");
    uEarthTexture = glGetUniformLocation(m_Program.getGLId(), "uEarthTexture");
    uCloudTexture = glGetUniformLocation(m_Program.getGLId(), "uCloudTexture");
  }
};

struct MoonProgram {
  Program m_Program;

  GLint uMVPMatrix;
  GLint uMVMatrix;
  GLint uNormalMatrix;
  GLint uTexture;

  MoonProgram(const FilePath &applicationPath)
      : m_Program(
            loadProgram(applicationPath.dirPath() + "shaders/3D.vs.glsl",
                        applicationPath.dirPath() + "shaders/tex3D.fs.glsl")) {
    uMVPMatrix = glGetUniformLocation(m_Program.getGLId(), "uMVPMatrix");
    uMVMatrix = glGetUniformLocation(m_Program.getGLId(), "uMVMatrix");
    uNormalMatrix = glGetUniformLocation(m_Program.getGLId(), "uNormalMatrix");
    uTexture = glGetUniformLocation(m_Program.getGLId(), "uTexture");
  }
};

float computeDistance(glm::ivec2 a, glm::ivec2 b)
{
  return pow(a.x - b.x, 2) + pow(a.y - b.y, 2);
}

int main(int argc, char** argv) {
  // Initialize SDL and open a window
  SDLWindowManager windowManager(800, 600, "GLImac");

  // Initialize glew for OpenGL3+ support
  GLenum glewInitError = glewInit();
  if(GLEW_OK != glewInitError) {
    std::cerr << glewGetErrorString(glewInitError) << std::endl;
    return EXIT_FAILURE;
  }

  FilePath applicationPath(argv[0]);
  MoonProgram moonProgram(applicationPath);
  EarthProgram earthProgram(applicationPath);

  std::cout << "OpenGL Version : " << glGetString(GL_VERSION) << std::endl;
  std::cout << "GLEW Version : " << glewGetString(GLEW_VERSION) << std::endl;

  /*********************************
   * HERE SHOULD COME THE INITIALIZATION CODE
   *********************************/

  std::unique_ptr<Image> earth_map = loadImage("/home/nicolas/Documents/IMAC/SEM3/imac-s3-opengl/TPs/TP3/assets/textures/EarthMap.jpg");
  std::unique_ptr<Image> moon_map = loadImage("/home/nicolas/Documents/IMAC/SEM3/imac-s3-opengl/TPs/TP3/assets/textures/MoonMap.jpg");
  std::unique_ptr<Image> cloud_map = loadImage("/home/nicolas/Documents/IMAC/SEM3/imac-s3-opengl/TPs/TP3/assets/textures/CloudMap.jpg");

  GLuint earth_map_tex, moon_map_tex, cloud_map_tex;
  glGenTextures(1, &earth_map_tex);
  glGenTextures(1, &moon_map_tex);
  glGenTextures(1, &cloud_map_tex);

  glBindTexture(GL_TEXTURE_2D, earth_map_tex);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, earth_map->getWidth(), earth_map->getHeight(), 0, GL_RGBA, GL_FLOAT, earth_map->getPixels());
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  glBindTexture(GL_TEXTURE_2D, moon_map_tex);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, moon_map->getWidth(), moon_map->getHeight(), 0, GL_RGBA, GL_FLOAT, moon_map->getPixels());
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  glBindTexture(GL_TEXTURE_2D, cloud_map_tex);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, cloud_map->getWidth(), cloud_map->getHeight(), 0, GL_RGBA, GL_FLOAT, cloud_map->getPixels());
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  glBindTexture(GL_TEXTURE_2D, 0);

  earthProgram.m_Program.use();
  earthProgram.uEarthTexture = glGetUniformLocation(earthProgram.m_Program.getGLId(), "uTexture1");
  earthProgram.uCloudTexture = glGetUniformLocation(earthProgram.m_Program.getGLId(), "uTexture2");

  moonProgram.m_Program.use();
  moonProgram.uTexture = glGetUniformLocation(moonProgram.m_Program.getGLId(), "uTexture");

  std::vector<glm::vec3> positions;
  for ( int i = 0; i < 32; i++ ) {
    positions.push_back(glm::sphericalRand(2.f));
  }

  glm::vec3 test = glm::sphericalRand(2.f);

  Sphere sphere(1, 32, 16);
  // Création d'un VBO
  GLuint vbo;
  glGenBuffers(1, &vbo);

  // Binding d'un VBO sur la cible GL_ARRAY_BUFFER
  glBindBuffer(GL_ARRAY_BUFFER, vbo);

  glBufferData(GL_ARRAY_BUFFER, sphere.getVertexCount() * sizeof(ShapeVertex), sphere.getDataPointer(), GL_STATIC_DRAW);

  // Débinder le VBO
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  // Spécification de sommet avec le Vertex Array Object (VAO)
  GLuint vao;
  glGenVertexArrays(1, &vao);

  // Binding du VAO
  glBindVertexArray(vao);

  // Activation des attributs de vertex
  const GLuint VERTEX_ATTR_POSITION = 0;
  glEnableVertexAttribArray(VERTEX_ATTR_POSITION);
  const GLuint VERTEX_ATTR_NORMAL = 1;
  glEnableVertexAttribArray(VERTEX_ATTR_NORMAL);
  const GLuint VERTEX_ATTR_TEXCOORD = 2;
  glEnableVertexAttribArray(VERTEX_ATTR_TEXCOORD);

  // Spécification des attributs de vertex
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glVertexAttribPointer(VERTEX_ATTR_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const void *) 0);
  glVertexAttribPointer(VERTEX_ATTR_NORMAL, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const void *) offsetof(ShapeVertex, normal));
  glVertexAttribPointer(VERTEX_ATTR_TEXCOORD, 2, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const void *) offsetof(ShapeVertex, texCoords));
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  // Debinder le VAO
  glBindVertexArray(0);

  glEnable(GL_DEPTH_TEST);

  glm::mat4 ProjMatrix;
  glm::mat4 MVMatrix;
  glm::mat4 NormalMatrix;

  ProjMatrix = glm::perspective(glm::radians(70.f), 8.f/6.f, 0.1f, 100.f);
  MVMatrix = glm::translate(glm::mat4(), glm::vec3(0.f, 0.f, -5.f));
  NormalMatrix = glm::transpose(glm::inverse(MVMatrix));

  glm::ivec2 lastMousePosition;
  bool mousePressed = false;
  TrackballCamera trackballCamera;

  // Application loop:
  bool done = false;
  while(!done) {
    // Event loop:
    SDL_Event e;
    while(windowManager.pollEvent(e)) {
      if(e.type == SDL_QUIT) {
        done = true; // Leave the loop after this iteration
      }
      else if ( e.type == SDL_MOUSEWHEEL ) {
        trackballCamera.moveFront(e.wheel.y);
      }
    }

    /*********************************
     * HERE SHOULD COME THE RENDERING CODE
     *********************************/

    if ( mousePressed == false && windowManager.isMouseButtonPressed(SDL_BUTTON_LEFT) ) {
      mousePressed = true;
      lastMousePosition = windowManager.getMousePosition();
    } else if ( mousePressed == true && windowManager.isMouseButtonPressed(SDL_BUTTON_LEFT) ) {
      glm::ivec2 newMousePosition = windowManager.getMousePosition();
      if ( lastMousePosition != newMousePosition ) {
        trackballCamera.rotateUp(newMousePosition.x - lastMousePosition.x);
        trackballCamera.rotateLeft(newMousePosition.y - lastMousePosition.y);
        lastMousePosition = newMousePosition;
        std::cout << newMousePosition << std::endl;
        std::cout << "pos x:" << newMousePosition.x - lastMousePosition.x << std::endl;
        std::cout << "pos y:" << newMousePosition.y - lastMousePosition.y << std::endl;
      }
    } else if ( mousePressed == true && windowManager.isMouseButtonPressed(SDL_BUTTON_LEFT) == false) {
      mousePressed = false;
    }

    glm::mat4 viewMatrix = MVMatrix * trackballCamera.getViewMatrix();
    NormalMatrix = glm::transpose(glm::inverse(viewMatrix));

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    earthProgram.m_Program.use();
    glUniform1i(earthProgram.uEarthTexture, 0);
    glUniform1i(earthProgram.uCloudTexture, 1);

    glUniformMatrix4fv(earthProgram.uMVPMatrix, 1, GL_FALSE, glm::value_ptr(ProjMatrix * viewMatrix));
    glUniformMatrix4fv(earthProgram.uNormalMatrix, 1, GL_FALSE, glm::value_ptr(NormalMatrix));


    glm::mat4 earthMVMatrix = glm::rotate(NormalMatrix, windowManager.getTime(), glm::vec3(0, 1, 0));
    glUniformMatrix4fv(earthProgram.uMVMatrix, 1, GL_FALSE, glm::value_ptr(earthMVMatrix));

    glBindVertexArray(vao);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, earth_map_tex);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, cloud_map_tex);

    glDrawArrays(GL_TRIANGLES, 0, sphere.getVertexCount());

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, 0);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, 0);

    moonProgram.m_Program.use();
    glUniformMatrix4fv(moonProgram.uMVPMatrix, 1, GL_FALSE, glm::value_ptr(ProjMatrix * viewMatrix));
    glUniformMatrix4fv(moonProgram.uNormalMatrix, 1, GL_FALSE, glm::value_ptr(NormalMatrix));

    glActiveTexture(GL_TEXTURE0);
    for ( int i = 0; i < positions.size(); i++ ) {
      glm::mat4 moonMVMatrix = glm::translate(glm::mat4(1), glm::vec3(0, 0, -5)); // Translation
      moonMVMatrix = glm::rotate(moonMVMatrix, windowManager.getTime() * positions[i].x, positions[i]); // Translation * Rotation
      moonMVMatrix = glm::translate(moonMVMatrix, glm::vec3(7, 0, 0)); // Translation * Rotation * Translation
      moonMVMatrix = glm::scale(moonMVMatrix, glm::vec3(0.2, 0.2, 0.2)); // Translation * Rotation * Translation * Scale

      glUniformMatrix4fv(moonProgram.uMVMatrix, 1, GL_FALSE, glm::value_ptr(moonMVMatrix));
      glBindTexture(GL_TEXTURE_2D, moon_map_tex);
      glDrawArrays(GL_TRIANGLES, 0, sphere.getVertexCount());
      glBindTexture(GL_TEXTURE_2D, 0);
    }

    glBindVertexArray(0);

    // Update the display
    windowManager.swapBuffers();
  }

  return EXIT_SUCCESS;
}
