#version 330

in vec3 vVertexPosition;
in vec3 vVertexNormal;
in vec2 vVertexTextCoords;

out vec3 fFragColor;

void main() {
  fFragColor = normalize(vVertexNormal);
}
