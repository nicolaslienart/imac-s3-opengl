#version 330

in vec2 vVertexTextCoords;

uniform sampler2D uTexture;

out vec3 fFragColor;

void main() {
  vec3 texture_color = texture(uTexture, vVertexTextCoords).xyz;
  fFragColor = texture_color;
}
